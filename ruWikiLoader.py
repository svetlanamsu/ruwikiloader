from corus import load_wiki
from mysql_coonector import DBConnection
import pandas as pd

from wikiParser import WikiParser
from ruWikiLoaderException import ruWikiLoaderException

import logging

# настройка логирования
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M',
                    filename='data/ru_wiki_loader.log',
                    filemode='w'
                    )
console = logging.StreamHandler()
console.setLevel(logging.INFO)
logging.getLogger('').addHandler(console)
logger = logging.getLogger('ruWikiLoader')


def main():
    max_records = 1
    path = 'data/ruwiki-latest-pages-articles.xml.bz2'
    categories = ('Город', 'Населённые_пункты_по_алфавиту',
                  'Здоровье', 'Детские_болезни', 'Профессиональные_заболевания', 'Википедия:Избранные_статьи_по_медицине'
                  'Учёные_по_алфавиту', 'Музыканты_по_алфавиту', 'Персоналии_по_алфавиту'
                  )
    valid_sections = pd.read_json('data/relation_cat_sec.json', orient='records').to_dict(orient='index')
    # контейнер для сбора полезной информации
    data = pd.DataFrame()
    # parser
    wiki_parser = WikiParser()
    # загрузка статей
    records = load_wiki(path)
    count = 0
    with DBConnection() as connector:
        for article in records:
            try:
                # проверка на удовлетворение категории
                query = wiki_parser.get_exist_query(id=article.id, categories=categories)
                connector.cur.execute(query)
                article_categories = [i[1] for i in connector.cur.fetchall()]
                if not article_categories:
                    raise ruWikiLoaderException(
                        f'Not valid category by article_id = {article.id}, title = {article.title}.'
                    )
                # формирование признаков и целевых признаков
                key_cats = ','.join(article_categories)
                val_secs = valid_sections.get(key_cats)
                val_secs = val_secs['sections'] if val_secs else val_secs
                feat, tar, one_section_labels = wiki_parser.process_articles(
                    article=article.text,
                    valid_sections=val_secs
                )

                data = data.append({
                    'raw_text': article.text,
                    'sentences': feat,
                    'labels': tar,
                    'paper_id': article.id,
                    'article_categories': article_categories,
                    'main_sections': one_section_labels
                }, ignore_index=True)
                count += 1
                logger.info(f'{"="*10}Add {count} articles{"="*10}')
                if count == max_records:
                    break
            except ruWikiLoaderException as ex:
                logger.warning(ex)
                continue
    # финальное сохранение
    logger.info('Export in file....')
    file_name = f'data/ruwiki_data_{data.shape[0]}'
    data.to_csv(f'{file_name}.csv')
    data.to_json(f'{file_name}.json', orient='records')


if __name__ == '__main__':
    logger.info('Start ruWikiLoader.')
    main()
    logger.info('Finished.')
