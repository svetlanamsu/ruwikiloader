import re
from razdel import sentenize
from dataclasses import dataclass
import numpy as np

from ruWikiLoaderException import ruWikiLoaderException


@dataclass
class WikiParser:
    min_sections_one_level: int = 4
    max_sections_one_level: int = 6
    min_sentences_in_segment: int = 2
    max_sentences_in_segment: int = 15

    def __post_init__(self):
        self.rubbish_labels = [
            'Города-побратимы',
            'См. также',
            'Литература',
            'Ссылки',
            'Примечания',
            'Статьи',
            'Книги'
        ]

    @staticmethod
    def get_section_separator(level):
        """
        :param level: уровень раздела
        :return: pattern для данного уровня заголовка
        """
        return r'(?:(?<=[^=])|^)={' + str(level) + r'}[^=]+={' + str(level) + r'}'

    @staticmethod
    def get_pattern_section_label(level):
        return r'(?<=={' + str(level) + r'}\s).+(?=\s={' + str(level) + '})'

    def get_sections(self, text):
        """
        Разделение теста на секции
        :param text: текст статьи
        :return: лист с разделами статьи
        """
        sections = list()
        one_sections = re.split(self.get_section_separator(2), text)  # разделы 1-го уровня
        if len(one_sections) <= self.min_sections_one_level:
            return None, None
        # labels - названия разделов
        # первый раздел идет без заголовка
        one_sections_labels = (re.search(self.get_pattern_section_label(2), i).group(0)
                               for i in re.findall(self.get_section_separator(2), text))
        out_labels = list()
        # 1 - введение
        try:
            for section, label in zip(one_sections[1:], one_sections_labels):
                if not (label in self.rubbish_labels):  # если это не мусорный раздел то добавляем его
                    sections.extend(re.split(self.get_section_separator(3), section))
                    out_labels.append(label)
        except Exception as ex:
            ruWikiLoaderException(f'Parsing error:{ex}')
        return sections, out_labels

    @staticmethod
    def create_features_target(section):
        """
        Разделение сегмента по предложениям и добавление label
        :param section: одна секция
        :return: список предложений, список labels
        """
        sentences = sentenize(section)
        features = list()
        for sent in sentences:
            features.append(sent.text)
        target = np.zeros(len(features))
        target[-1] = 1
        return features, target

    def process_articles(self, article, valid_sections=None):
        """
        Для всей статьи составить список предложений с метками
        :param article: текст статьи
        :param valid_sections: разделы, которые нужно взять в оборот
        :return: список предложений, labels
        """
        if re.search(r'[^=]={4}[^=]', article):  # раздел более высокого порядка
            raise ruWikiLoaderException('Article include 3-level subtitle.')
        features = list()
        target = np.empty(shape=0)
        sections, one_level_labels = self.get_sections(article)
        if sections is None:
            raise ruWikiLoaderException("This article has not valid section.")
        for section, one_level_label in zip(sections, one_level_labels):
            if valid_sections is None or one_level_label in valid_sections:
                f, t = self.create_features_target(section)
            else:
                continue
            # если в разделе мало или очень много предложений
            if not (self.min_sentences_in_segment <= t.shape[0] < self.max_sentences_in_segment):
                continue
            features.extend(f)
            target = np.concatenate((target, t))
        if target.sum() < 2:
            raise ruWikiLoaderException('The article contains less than two sections.')
        return features, target, one_level_labels

    @staticmethod
    def get_exist_query(id, categories):
        q = 'SELECT cl_from, CAST(cl_to AS CHAR(300)) as category, cl_timestamp, cl_type FROM categorylinks ' + \
        f'WHERE cl_from={id} AND CAST(cl_to AS CHAR(300)) in {categories};'
        return q
