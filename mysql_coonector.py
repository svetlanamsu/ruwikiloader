from mysql import connector


class DBSettings:
    user = 'root'
    password = '123456789'
    host = '127.0.0.1'
    port = '3306'
    db_name = 'ruwiki'


class DBConnection:
    def __init__(self):
        self.mydb = connector.connect(
            host=DBSettings.host,
            user=DBSettings.user,
            password=DBSettings.password,
            database=DBSettings.db_name,
            port=DBSettings.port
        )
        self.cur = self.mydb.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # close db connection
        self.cur.close()


if __name__ == '__main__':
    with DBConnection() as connect:
        connect.cur.execute("""
        SELECT
            cl_from,
            CAST(cl_to AS CHAR(300)) as category,
            cl_timestamp,
            cl_type
        FROM 
            categorylinks 
        WHERE
            cl_from=116;
        """)
        my_result = connect.cur.fetchall()
        print('len x =', len(my_result))
        for x in my_result:
            print(x)
