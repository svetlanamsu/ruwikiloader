# ruWikiLoader

## Описание
Загрузка и обработка дампа русской википедии для задачи сегментации.

## Структура
- **mysql_connector.py** - работа с СУБД mysql.
	- `DBSettings` - настройки подключения к базе данных(user, password, host, port, database name). В базе должен быть развернут дамп *categorylinks*, который содержит связь между id статьи и категорией статьи. 
	- `DBConnection` - контекстный менеджер для рабооты с СУБД mysql.
- **ruWikiLoaderException.py** - кастомный класс исключений.
- **wikiParser.py** - класс для извлечения признаков для задачи сегментации. Настройки:
	- `min_sections_one_level` - минимальное количество разделов первого уровня
	- `max_sections_one_level` - максимальное количество разделов первого уровня
	- `min_sentences_in_segment` - минимальное количество предложений в одном разделе
	- `max_sentences_in_segment` - максимальное предложений в одном разделе

	Принцип работы:
	- Выкидывается исключение типа `ruWikiLoaderException` при наличии разделов 3-го уровня.
	- Выкидываются разделы из числа мусорных: 'Города-побратимы', 'См. также', 'Литература', 'Ссылки', 'Примечания', 'Статьи', 'Книги'.
	- Для каждой категории составлен топ общих разделов(`relation_cat_sec.json`). Для каждой категории берутся разделы только из этого списка.
	- В разделе для каждого предложения проставляется бинарная метка: 0, 1. 1 - индикатор последнего предложения в разделе.
- **ruWikiLoader.py** - точка входа.
Параметры:
	- `max_records` - максимальное количество необходимых записей.
	- `path` - путь к дампу русской википедии. [dump](https://dumps.wikimedia.org/ruwiki/latest/) 
	- `categories` - категории, который необходимо извлечь.
	- `valid_sections` - словарь соответствия категорий и частых разделов.

	Сохранение получившихся данных в 2 фомата `csv, json`.
	Поля:
	- `raw_text` - необдработанный текст статьи
    - `sentences` - лист из предложений
    - `labels` - лист с бинарными метками
    - `paper_id` - id статьи
    - `article_categories` - категории к которым пренадлежит статья
    - `main_sections` - названия разделов первого уровня
